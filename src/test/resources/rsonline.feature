Feature: Five Test Scenarios for category page on rs components.

  Background: 
    Given user is on the category page

  Scenario: Verify Menu Header on the Category Page
    When user navigates to category page
      | RS Components |
    Then user should be able to see the menu header elements
      | All Products | Our Brands | New Products | My Account | Our Services |

  Scenario: Verify Batteries Page
    When user clicks on Batteries link
    Then Batteries Page shoud be displayed
      | Batteries |

  Scenario: Verify the product category-OfficeSupply page
    When user clicks on office supplies link
    Then user verifies All office Supplies products should be displayed
 

  Scenario: Add one office-supplies category product and verify the basket contains that product.
    When user adds office-supplies product
    Then user should be able to see product in the basket

  Scenario: Verify connectors page
    When user clicks on Connectorslink
    Then Connectors page should be displayed
    | Connectors |
    
   
