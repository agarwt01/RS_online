package com.RSonline.Pages;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.RSonline.Utils.Utils;

public class CategoryPage extends BasePage{
    
   

   
    
    @FindBy(how=How.XPATH, using = "//a[@title='All Products']")
    WebElement allProductsLink;
    
    @FindBy(how=How.XPATH, using = "//a[@title='Our Brands']")
    WebElement ourBrandsLink;
    
    @FindBy(how=How.XPATH, using = "//a[@title='New Products']")
    WebElement newProductsLink;
    
    @FindBy(how=How.XPATH, using = "//a[@title='My Account']")
    WebElement myAccountLink;
    
    @FindBy(how=How.XPATH, using = "//a[@title='Our Services']")
    WebElement ourServicesLink;
    
    @FindBy(how=How.XPATH, using = "//a[@title='Batteries']")
    WebElement batteries;
    
    @FindBy(how=How.CSS, using = ".rs_gaTrackEvent[title='Batteries']")
    WebElement batterieslink;
    
    
    @FindBy(how=How.CSS, using = "h1")
    WebElement headerbatteries;

    
    @FindBy(how=How.XPATH, using = "//a[@title='Office Supplies']")
    WebElement officeSupplies;
    
    @FindBy(how=How.CSS, using = "#srtnLayout.newCatPage")
    WebElement allProductsOfficeSupplies;
    
    @FindBy(how=How.XPATH, using = "//input[@class='cartButton']")
    List<WebElement> addItem;
    
    @FindBy(how=How.CSS, using = ".qty")
    WebElement viewBasketButton;
    
    @FindBy(how=How.CSS, using = ".green.header")
    WebElement stockOrderSummary;
    
   @FindBy(how=How.CSS, using = "#clearBasketButton")
    WebElement clearBasketButton;
   
   
   @FindBy(how=How.CSS, using = "#confirmDeleteContinue")
    WebElement confirmDelete;
   
   @FindBy(how=How.CSS, using = "#shoppingBasketForm:WebBasketLineWidgetActionVALIDATION_ERROR_EVENT")
   WebElement confirmationDeletedProducts;
 
   
    @FindBy(how=How.CSS, using = "h1")
    WebElement logIn;
   
    @FindBy(how=How.CSS, using = ".acsInviteButton acsDeclineButton")
    WebElement popupdismiss;
   
    @FindBy(how=How.XPATH, using = "//a[@title='Connectors']")
    WebElement connectors;
    
    @FindBy(how=How.CSS, using = ".rs_gaTrackEvent[title='Connectors']")
    WebElement connectorslink;

    @FindBy(how=How.CSS, using = "h1")
    WebElement headerConnectors;

  
    
  public void clearBasket()
  {
	  
	  clearBasketButton.click();
	  
	  
	  confirmDelete.click();
	
  }
    
  
  public void clickOnCategoryConnectors(){
  	connectors.click();
  	connectorslink.click();
  	}
  
  
  public String getHeaderConnectors(){
	   
  	Utils.waitForElementVisible(headerConnectors);
  	System.out.println(headerConnectors.getText());
  	return headerConnectors.getText();
  	
  	}
    
    public void addToCart()
    {
    	addItem.get(0).click();
    	
    	viewBasketButton.click();
    	
    }
    
    
    
   public boolean getAvailability()
   {
	   
	   System.out.println(stockOrderSummary.getText().contains("In stock"));
	   return stockOrderSummary.getText().contains("In stock");
	  
	   
   }
    
   public String getSummaryText()
   {
	   return stockOrderSummary.getText();
   }
    
   
   
   public String getSummaryDetails()
   {
	   return confirmationDeletedProducts.getText();
   }
    
    
    public void clickOnCategoryBattery(){
    	batteries.click();
    	batterieslink.click();
    	}
    
    
    public String getHeader(){
   
    	Utils.waitForElementVisible(headerbatteries);
    	System.out.println(headerbatteries.getText());
    	return headerbatteries.getText();
    	
    	}
   
    
    public void clickOnCategoryOfficeSuplies(){
    	officeSupplies.click();
    	}
    
    
    public boolean isDisplayed(){
   
    	Utils.waitForElementVisible(allProductsOfficeSupplies);
    	System.out.println(allProductsOfficeSupplies.getText().contains("Viewing"));
    	return allProductsOfficeSupplies.isDisplayed();
    	
    	}
    
    
    public CategoryPage(){
        PageFactory.initElements(driver,this);
    }
   

    public String getPageTitle(){
    	System.out.println(driver.getTitle());
    	return driver.getTitle();
    }
    
    public String isAllProductsLinkDisplayed(){
    	Utils.waitForElementVisible(allProductsLink);
    	System.out.println(allProductsLink.getText());
    	return allProductsLink.getText();
    }
    
    public String isOurBrandsLinkDisplayed(){
    	Utils.waitForElementVisible(ourBrandsLink);
    	System.out.println(ourBrandsLink.getText());
    	return ourBrandsLink.getText();
    }
    
    public String isNewProductsLinkDisplayed(){
    	Utils.waitForElementVisible(newProductsLink);
    	System.out.println(newProductsLink.getText());
    	return newProductsLink.getText();
    }
    
    public String isMyAccountLinkDisplayed(){
    	Utils.waitForElementVisible(myAccountLink);
    	System.out.println(myAccountLink.getText());
    	return myAccountLink.getText();
    }
    
    public String isOurServicesLinkDisplayed(){
    	Utils.waitForElementVisible(ourServicesLink);
    	System.out.println(ourServicesLink.getText());
    	return ourServicesLink.getText();
    }
}
