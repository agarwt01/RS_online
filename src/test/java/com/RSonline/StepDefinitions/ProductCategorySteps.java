package com.RSonline.StepDefinitions;

import org.junit.Assert;

import com.RSonline.Pages.CategoryPage;
import com.RSonline.Utils.BrowserFactory;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class ProductCategorySteps {
	
	public CategoryPage categoryPage;
	
	
	@Before
    public void setUp(){
        BrowserFactory.startBrowser();
    }
    @After
    public void cleanUp(Scenario scenario){
       BrowserFactory.closeBrowser();
    }
    
    
	@Given("^user is on the category page$")
	public void user_is_logged_on_the_home_page() throws Throwable {
		categoryPage = new CategoryPage();
		
	}

	@When("^user navigates to category page$")
	public void user_logs_in_through_category_page(DataTable pageTitle) throws Throwable {
		Assert.assertTrue(categoryPage.getPageTitle().contains(pageTitle.asList(String.class).get(0)));
	}

	@Then("^user should be able to see the menu header elements$")
	public void user_is_able_to_see_the_login_name_as_specified(DataTable menu) throws Throwable {
		Assert.assertTrue(categoryPage.isAllProductsLinkDisplayed().equalsIgnoreCase(menu.asList(String.class).get(0)));
		Assert.assertTrue(categoryPage.isOurBrandsLinkDisplayed().equalsIgnoreCase(menu.asList(String.class).get(1)));
		Assert.assertTrue(categoryPage.isNewProductsLinkDisplayed().equalsIgnoreCase(menu.asList(String.class).get(2)));
		Assert.assertTrue(categoryPage.isMyAccountLinkDisplayed().equalsIgnoreCase(menu.asList(String.class).get(3)));
		Assert.assertTrue(categoryPage.isOurServicesLinkDisplayed().equalsIgnoreCase(menu.asList(String.class).get(4)));
	}

	@When("^user clicks on Batteries link$")
	public void user_click_on_Batteries() throws Throwable {
	   categoryPage.clickOnCategoryBattery();
	}

	@Then("^Batteries Page shoud be displayed$")
	public void user_able_to_see_Batteries_Page(DataTable pageHeader) throws Throwable {
		Assert.assertTrue(categoryPage.getHeader().contains(pageHeader.asList(String.class).get(0)));
	}

	@When("^user clicks on office supplies link$")
	public void user_clicks_on_office_supplies_link() throws Throwable {
	    categoryPage.clickOnCategoryOfficeSuplies();
	}

	@Then("^user verifies All office Supplies products should be displayed$")
	public void user_verifies_All_office_Supplies_products_should_be_displayed() throws Throwable {
	   Assert.assertTrue(categoryPage.isDisplayed());
	}
	

	@When("^user adds office-supplies product$")
	public void user_adds_office_supplies_product() throws Throwable {
		categoryPage.clickOnCategoryOfficeSuplies();
		Assert.assertTrue(categoryPage.isDisplayed());
		categoryPage.addToCart();
	}

	@Then("^user should be able to see product in the basket$")
	public void user_should_be_able_to_see_product_in_the_basket() throws Throwable {
	    categoryPage.getAvailability();
	    Assert.assertTrue(categoryPage.getSummaryText().contains("In stock"));
	}

	@When("^user clicks on Connectorslink$")
	public void user_clicks_on_Connectorslink() throws Throwable {
		categoryPage.clickOnCategoryConnectors();
	}

	@Then("^Connectors page should be displayed$")
	public void Connectors_page_should_be_displayed(DataTable pageHeader) throws Throwable {
		Assert.assertTrue(categoryPage.getHeaderConnectors().contains(pageHeader.asList(String.class).get(0)));
	}

}
